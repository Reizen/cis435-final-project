### SQL Server Lab Final Project
---

### Database

| Stored Procedure    | Status     |
| ------------------- | ---------- |
| SP_CREATE_EMPLOYEE  | `Complete` |
| SP_EDIT_EMPLOYEE    | `Complete` |
| SP_DELETE_EMPLOYEE  | `Complete` |

### Forms

| Task			        | Status     |
| --------------------- | ---------- |
| MetroUI			    | `Complete` |
| Create Employee       | `Complete` |
| Edit Employee         | `Complete` |
| Delete Employee       | `Complete` |
| Foreign Key Drop Down | `Complete` |

| Task			               | Status   	  |
| ---------------------------- | ------------ |
| Enter Hours for Project      | Working  	  |
| Employee & Project Drop Down | Working  	  |
| Date & Hours Textboxes       | Working  	  |
| List Employees	    	   | `Completed`  |
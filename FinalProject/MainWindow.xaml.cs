﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Data;

namespace FinalProject
{
    public partial class MainWindow : MetroWindow
    {
        private List<Employee> empList;
        private Employee editEmp;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
            tblkDeletePending.Text = "";
        }

        private void LoadData()
        {
            empList = new List<Employee>();

            CIS435LProjDataSetTableAdapters.EmployeeTableAdapter eta = new CIS435LProjDataSetTableAdapters.EmployeeTableAdapter();

            CIS435LProjDataSet.EmployeeDataTable dt = eta.GetData();

            foreach (CIS435LProjDataSet.EmployeeRow dr in dt.Rows)
                empList.Add(new Employee(
                    dr.emp_id, dr.emp_firstname, dr.emp_lastname, dr.emp_socialsec,
                    dr.emp_address, dr.emp_city, dr.emp_state, dr.emp_gender, dr.emp_birthdate,
                    (dr["emp_supervisorid"] == DBNull.Value) ? -1 : dr.emp_supervisorid));

            cbSupervisor.Items.Clear();
            cbSupervisor_Edit.Items.Clear();

            foreach (Employee emp in empList)
            {
                cbSupervisor.Items.Add(emp.FirstName + " " + emp.LastName);
                cbSupervisor_Edit.Items.Add(emp.FirstName + " " + emp.LastName);
            }
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            // validate first name and last name
            if (!string.IsNullOrWhiteSpace(tbFirstName.Text) && !string.IsNullOrWhiteSpace(tbLastName.Text))
            {
                CIS435LProjDataSetTableAdapters.QueriesTableAdapter qs = new CIS435LProjDataSetTableAdapters.QueriesTableAdapter();
                int ret = 0;
                try
                {
                    ret = qs.SP_CREATE_EMPLOYEE(tbFirstName.Text,
                                                tbLastName.Text,
                                                (tbSocial.Text == "") ? null : tbSocial.Text,
                                                (tbAddress.Text == "") ? null : tbAddress.Text,
                                                (tbCity.Text == "") ? null : tbCity.Text,
                                                (tbState.Text == "") ? null : tbState.Text,
                                                (cbGender.SelectedIndex == -1) ? null : (cbGender.SelectedIndex == 0) ? "M" : "F",
                                                dpBirthDate.SelectedDate,
                                                (cbSupervisor.SelectedIndex == -1) ? (int?)null : (empList[cbSupervisor.SelectedIndex].id));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                if (ret == 1)
                {
                    this.ShowMessageAsync("Create Employee", "Employee " + tbFirstName.Text + " " + tbLastName.Text + " has been created!");
                    ClearForms(createGridView);
                    LoadData();
                }
                else
                    this.ShowMessageAsync("Create Employee", "An error was caught when attempting to create an employee.");
            }
            else
            {
                // Show error message
                this.ShowMessageAsync("Invalid value", "FirstName and LastName cannot be null!");
            }
        }

        private void ClearForms(Grid g)
        {
            foreach (UIElement ele in g.Children)
            {
                TextBox tbox;
                ComboBox cbox;
                DatePicker dpick;

                if (ele.GetType() == typeof(TextBox))
                {
                    tbox = (TextBox)ele;
                    tbox.Text = string.Empty;
                }
                else if (ele.GetType() == typeof(ComboBox))
                {
                    cbox = (ComboBox)ele;
                    cbox.SelectedIndex = -1;
                }
                else if (ele.GetType() == typeof(DatePicker))
                {
                    dpick = (DatePicker)ele;
                    dpick.SelectedDate = null;
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabCtrl = (TabControl)sender;

            if (tabCtrl.SelectedIndex == 1 || tabCtrl.SelectedIndex == 2)
                flyEmployeeLookup.IsOpen = true;
            else
                flyEmployeeLookup.IsOpen = false;
        }

        private void btFind_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(lookupFirstName.Text) && !string.IsNullOrWhiteSpace(lookupLastName.Text))
            {
                editEmp = empList.Find(item => (item.FirstName == lookupFirstName.Text) && (item.LastName == lookupLastName.Text));

                if (editEmp == null)
                    this.ShowMessageAsync("Employee Lookup", "Couldn't find " + lookupFirstName.Text +  " " + lookupLastName.Text + ".");
                else
                {
                    if (tcMain.SelectedIndex == 1)
                        PrepareEditEmployee();
                    else
                        PrepareDeleteEmployee();
                }
            }
            else
                this.ShowMessageAsync("Employee Lookup", "In order to search for an employee you must enter a first name and last name!");
        }

        private void PrepareEditEmployee()
        {
            tbFirstName_Edit.Text = editEmp.FirstName;
            tbLastName_Edit.Text = editEmp.LastName;
            tbSocial_Edit.Text = editEmp.Social;
            tbAddress_Edit.Text = editEmp.Address;
            tbCity_Edit.Text = editEmp.City;
            tbState_Edit.Text = editEmp.State;

            if (editEmp.Gender == "M")
                cbGender_Edit.SelectedIndex = 0;
            else if (editEmp.Gender == "F")
                cbGender_Edit.SelectedIndex = 1;

            dpBirthDate_Edit.SelectedDate = editEmp.BirthDate;
            if (editEmp.Supervisor == -1)
                cbSupervisor_Edit.SelectedIndex = -1;
            else
                cbSupervisor_Edit.SelectedIndex = (empList.Find(item => item.id == editEmp.Supervisor)).Supervisor;

            flyEmployeeLookup.IsOpen = false;
        }

        private void btEdit_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbFirstName_Edit.Text) && !string.IsNullOrWhiteSpace(tbLastName_Edit.Text))
            {
                CIS435LProjDataSetTableAdapters.QueriesTableAdapter qs = new CIS435LProjDataSetTableAdapters.QueriesTableAdapter();
                int ret = 0;
                ret = qs.SP_EDIT_EMPLOYEE(editEmp.id, 
                                          tbFirstName_Edit.Text, 
                                          tbLastName_Edit.Text, 
                                          tbSocial_Edit.Text.ToString(), 
                                          tbAddress_Edit.Text, 
                                          tbCity_Edit.Text,
                                          tbState_Edit.Text, 
                                          (cbGender_Edit.SelectedIndex == 0) ? "M" : "F", 
                                          dpBirthDate_Edit.SelectedDate,
                                          (editEmp.Supervisor == -1) ? (int?)null : editEmp.Supervisor);

                if (ret == 1)
                {
                    this.ShowMessageAsync("Edit Employee", "Employee " + tbFirstName_Edit.Text + " " + tbLastName_Edit.Text + " has been edited!");
                    ClearForms(editGridView);
                }
                else
                    this.ShowMessageAsync("Edit Employee", "There was an error editing employee!");
            }
            else
                this.ShowMessageAsync("Edit Employee", "First Name and Last Name cannot be null!");
        }

        private void PrepareDeleteEmployee()
        {
            tblkDeletePending.Text = editEmp.FirstName + " " + editEmp.LastName + " deletion pending.";

            flyEmployeeLookup.IsOpen = false;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (tblkDeletePending.Text != "")
            {
                CIS435LProjDataSetTableAdapters.QueriesTableAdapter qs = new CIS435LProjDataSetTableAdapters.QueriesTableAdapter();
                int ret = 0;
                ret = qs.SP_DELETE_EMPLOYEE(editEmp.id);

                tblkDeletePending.Text = "";

                this.ShowMessageAsync("Delete Employee", "Employee " + editEmp.FirstName + " " + editEmp.LastName + " has been deleted!");
                LoadData();
            }
        }

        private void btDataView_Click(object sender, RoutedEventArgs e)
        {
            DataViewWindow dvw = new DataViewWindow();
            dvw.Show();
        }
    }
}
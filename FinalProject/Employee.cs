﻿using System;
public class Employee
{
    public int id;
    public string FirstName;
    public string LastName;
    public string Social;
    public string Address;
    public string City;
    public string State;
    public string Gender;
    public DateTime BirthDate;
    public int Supervisor;

    public Employee(int id, string firstname, string lastname, string social, 
        string address, string city, string state, string gender, DateTime birth, int supervisor)
    {
        this.id = id;
        this.FirstName = firstname;
        this.LastName = lastname;
        this.Social = social;
        this.Address = address;
        this.City = city;
        this.State = state;
        this.Gender = gender;
        this.BirthDate = birth;
        this.Supervisor = supervisor;
    }
}
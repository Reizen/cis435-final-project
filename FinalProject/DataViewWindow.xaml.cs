﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FinalProject
{
    public partial class DataViewWindow : MetroWindow
    {
        public DataViewWindow()
        {
            InitializeComponent();

            CIS435LProjDataSetTableAdapters.EmployeeTableAdapter eta = new CIS435LProjDataSetTableAdapters.EmployeeTableAdapter();
            CIS435LProjDataSet cDS = new CIS435LProjDataSet();
            
            eta.Fill(cDS.Employee);

            dgMainGrid.ItemsSource = cDS.Tables["Employee"].DefaultView;
        }
    }
}

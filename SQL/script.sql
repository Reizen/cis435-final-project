USE [master]
GO
/****** Object:  Database [CIS435LProj]    Script Date: 12/5/2014 4:25:01 PM ******/
CREATE DATABASE [CIS435LProj]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CIS435LProj', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\CIS435LProj.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CIS435LProj_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\CIS435LProj_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CIS435LProj] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CIS435LProj].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CIS435LProj] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CIS435LProj] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CIS435LProj] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CIS435LProj] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CIS435LProj] SET ARITHABORT OFF 
GO
ALTER DATABASE [CIS435LProj] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [CIS435LProj] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CIS435LProj] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CIS435LProj] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CIS435LProj] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CIS435LProj] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CIS435LProj] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CIS435LProj] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CIS435LProj] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CIS435LProj] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CIS435LProj] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CIS435LProj] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CIS435LProj] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CIS435LProj] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CIS435LProj] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CIS435LProj] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CIS435LProj] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CIS435LProj] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CIS435LProj] SET  MULTI_USER 
GO
ALTER DATABASE [CIS435LProj] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CIS435LProj] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CIS435LProj] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CIS435LProj] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CIS435LProj] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CIS435LProj]
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Departments](
	[dept_id] [int] IDENTITY(1,1) NOT NULL,
	[dept_name] [varchar](25) NOT NULL,
	[mngr_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dependents]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dependents](
	[dep_id] [int] IDENTITY(1,1) NOT NULL,
	[dep_firstname] [varchar](25) NOT NULL,
	[dep_gender] [char](1) NULL,
	[dep_birthdate] [smalldatetime] NULL,
	[dep_relationship] [varchar](25) NULL,
	[emp_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[dep_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[emp_id] [int] IDENTITY(1,1) NOT NULL,
	[emp_firstname] [varchar](25) NOT NULL,
	[emp_lastname] [varchar](25) NOT NULL,
	[emp_socialsec] [varchar](11) NOT NULL,
	[emp_address] [varchar](25) NOT NULL,
	[emp_city] [varchar](25) NOT NULL,
	[emp_state] [char](2) NOT NULL,
	[emp_gender] [char](1) NOT NULL,
	[emp_birthdate] [date] NOT NULL,
	[emp_supervisorid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmpProj]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpProj](
	[empproj_id] [int] IDENTITY(1,1) NOT NULL,
	[empproj_hours] [int] NOT NULL,
	[emp_id] [int] NOT NULL,
	[proj_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[empproj_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Locations]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Locations](
	[loc_id] [int] IDENTITY(1,1) NOT NULL,
	[loc_name] [varchar](25) NOT NULL,
	[proj_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[loc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Projects]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Projects](
	[proj_id] [int] IDENTITY(1,1) NOT NULL,
	[proj_name] [varchar](25) NOT NULL,
	[dept_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[proj_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[EmpProj] ADD  DEFAULT ((0)) FOR [empproj_hours]
GO
ALTER TABLE [dbo].[Departments]  WITH CHECK ADD FOREIGN KEY([mngr_id])
REFERENCES [dbo].[Employee] ([emp_id])
GO
ALTER TABLE [dbo].[Dependents]  WITH CHECK ADD FOREIGN KEY([emp_id])
REFERENCES [dbo].[Employee] ([emp_id])
GO
ALTER TABLE [dbo].[EmpProj]  WITH CHECK ADD FOREIGN KEY([emp_id])
REFERENCES [dbo].[Employee] ([emp_id])
GO
ALTER TABLE [dbo].[EmpProj]  WITH CHECK ADD FOREIGN KEY([proj_id])
REFERENCES [dbo].[Projects] ([proj_id])
GO
ALTER TABLE [dbo].[Locations]  WITH CHECK ADD FOREIGN KEY([proj_id])
REFERENCES [dbo].[Projects] ([proj_id])
GO
ALTER TABLE [dbo].[Projects]  WITH CHECK ADD FOREIGN KEY([dept_id])
REFERENCES [dbo].[Departments] ([dept_id])
GO
ALTER TABLE [dbo].[Dependents]  WITH CHECK ADD CHECK  (([dep_gender]='M' OR [dep_gender]='F'))
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD CHECK  (([emp_gender]='M' OR [emp_gender]='F'))
GO
/****** Object:  StoredProcedure [dbo].[SP_CREATE_EMPLOYEE]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_CREATE_EMPLOYEE]
	@FirstName varchar(25),
	@LastName varchar(25),
	@Social varchar(11) NULL,
	@Address varchar(25) NULL,
	@City varchar(25) NULL,
	@State char(2) NULL,
	@Gender char(1) NULL,
	@Birth smalldatetime NULL,
	@Supervisor int NULL
AS

-- Validation
BEGIN
	IF @FirstName IS NULL
		RAISERROR('First Name cannot be null!', 16, 1);
	IF @LastName IS NULL
		RAISERROR('Last Name cannot be null!', 16, 1);
	IF @State IS NOT NULL
		BEGIN
			SET @State = UPPER(@State);
		END
	IF @Gender IS NOT NULL
		BEGIN
			SET @Gender = UPPER(@Gender);

			IF @Gender <> 'M' AND @Gender <> 'F'
				RAISERROR('Gender is not valid', 16, 1);
		END
	IF @Supervisor IS NOT NULL
		BEGIN
			IF @Supervisor NOT IN (SELECT emp_supervisorid FROM Employee WHERE emp_id = @Supervisor)
				RAISERROR('Supervisor ID is not valid', 16, 1);
		END
END

-- Creation
BEGIN
	INSERT INTO Employee
	(emp_firstname, emp_lastname, emp_socialsec, emp_address, emp_city, emp_state, emp_gender, emp_birthdate, emp_supervisorid)
	VALUES
	(@FirstName, @LastName, @Social, @Address, @City, @State, @Gender, @Birth, @Supervisor)
END




GO
/****** Object:  StoredProcedure [dbo].[SP_DELETE_EMPLOYEE]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_DELETE_EMPLOYEE]
	@ID int
AS
BEGIN
	IF @ID IN (SELECT emp_id FROM Employee)
	BEGIN
		DELETE FROM Employee
		WHERE emp_id = @ID;

		UPDATE Employee
		SET emp_supervisorid = NULL
		WHERE emp_supervisorid = @ID;
	END
	ELSE
		RAISERROR('That employee does not exist!', 16, 1);
END

GO
/****** Object:  StoredProcedure [dbo].[SP_EDIT_EMPLOYEE]    Script Date: 12/5/2014 4:25:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_EDIT_EMPLOYEE]
	@ID	int,
	@FirstName varchar(25),
	@LastName varchar(25),
	@Social varchar(11) NULL,
	@Address varchar(25) NULL,
	@City varchar(25) NULL,
	@State char(2) NULL,
	@Gender char(1) NULL,
	@Birth smalldatetime NULL,
	@Supervisor int NULL
AS

-- Validation
BEGIN
	SET @State = UPPER(@State);

	IF @Gender IS NOT NULL
	BEGIN
		SET @Gender = UPPER(@Gender);
		IF @Gender <> 'M' AND @Gender <> 'F'
			RAISERROR('Invalid Gender!', 16, 1);
	END
END

-- Update
BEGIN
	UPDATE Employee
	SET emp_firstname = ISNULL(@FirstName, emp_firstname),
		emp_lastname  = ISNULL(@LastName, emp_lastname),
		emp_socialsec = ISNULL(@Social, emp_socialsec),
		emp_address   = ISNULL(@Address, emp_address),
		emp_city	  = ISNULL(@City, emp_city),
		emp_state	  = ISNULL(@State, emp_state),
		emp_gender	  = ISNULL(@Gender, emp_gender),
		emp_birthdate = ISNULL(@Birth, emp_birthdate),
		emp_supervisorid = ISNULL(@Supervisor, emp_supervisorid)
	WHERE emp_id = @ID;
END



GO
USE [master]
GO
ALTER DATABASE [CIS435LProj] SET  READ_WRITE 
GO
